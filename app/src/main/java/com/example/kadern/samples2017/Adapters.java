package com.example.kadern.samples2017;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kadern.samples2017.models.User;

import java.util.ArrayList;

public class Adapters extends AppCompatActivity {

    public static final String TAG = "Adapters";
    ListView listView;

    String[] names = {"Bob", "Sally", "Betty"};
    ArrayList<User> users = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapters);
        listView = (ListView)findViewById(R.id.listView);

        users.add(new User("Bob", "bob@bob.com", User.Music.JAZZ, true));
        users.add(new User("Sally", "bob@bob.com", User.Music.JAZZ, true));
        users.add(new User("Betty", "bob@bob.com", User.Music.JAZZ, true));

        //example1();   // uses array of strings as the data source
        //example2();     // uses arraylist of Users as the data source
        example3();         // uses a custom view for each item in the users arraylist
    }

    public void example1(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(Adapters.this, "POS: " + position,Toast.LENGTH_SHORT).show();
                String selectedString = names[position];
            }
        });
    }

    public void example2(){
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1, users);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = users.get(position);
                Toast.makeText(Adapters.this, selectedUser.getFirstName(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void example3(){
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, R.layout.custom_user_list_item, R.id.lblFirstName, users){
            @Override
            public View getView(int position, View view, ViewGroup parent){
                View itemView = super.getView(position, view, parent);
                TextView lblFirstName = (TextView)itemView.findViewById(R.id.lblFirstName);
                CheckBox chkActive = (CheckBox)itemView.findViewById(R.id.chkActive);

                User u = users.get(position);
                lblFirstName.setText(u.getFirstName());
                chkActive.setChecked(u.isActive());

                // TODO:
                // 1. chkActive.setTag(u)
                // 2. hook up the listener on the checkbox (and use getTag() in the callback)

                return itemView;
            }
        };

        listView.setAdapter(adapter);
    }
}
