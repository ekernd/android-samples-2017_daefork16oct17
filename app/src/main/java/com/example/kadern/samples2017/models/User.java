package com.example.kadern.samples2017.models;

import java.io.Serializable;


/**
 * Created by kadern on 9/18/2017.
 */

public class User implements Serializable {
    public enum Music{COUNTRY, RAP, JAZZ};

    private String firstName;
    private String email;
    private Music favoriteMusic;
    private boolean active;


    public User(){
        
    }

    public User(String firstName, String email, Music favoriteMusic, boolean active) {
        this.firstName = firstName;
        this.email = email;
        this.favoriteMusic = favoriteMusic;
        this.active = active;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Music getFavoriteMusic() {
        return favoriteMusic;
    }

    public void setFavoriteMusic(Music favoriteMusic) {
        this.favoriteMusic = favoriteMusic;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString(){
        return String.format("FName: %s Email: %s Music: %s Active: %b", firstName, email, favoriteMusic, active);

    }

}
